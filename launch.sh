#!/bin/sh

rm /etc/nginx/conf.d/default.conf || :
sed -i 's/FORWARD_HOST/'"$FORWARD_HOST"'/' /etc/nginx/conf.d/auth.conf
sed -i 's/FORWARD_PORT/'"$FORWARD_PORT"'/' /etc/nginx/conf.d/auth.conf
sed -i 's|HTPASSWD|'"$HTPASSWD"'|' /etc/nginx/auth.htpasswd

nginx -g "daemon off;"
