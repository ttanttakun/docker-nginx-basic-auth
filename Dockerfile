FROM nginx:alpine

ENV HTPASSWD='foo:$apr1$odHl5EJN$KbxMfo86Qdve2FH4owePn.' \
    FORWARD_PORT=80 \
    FORWARD_HOST=web

WORKDIR /opt

COPY auth.conf /etc/nginx/conf.d
COPY auth.htpasswd /etc/nginx
COPY launch.sh ./
CMD ["./launch.sh"]
